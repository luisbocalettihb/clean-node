module.export = class Engine {
    constructor(dep) {
        this.dep = dep;
    }

    run = async () => {
        var products = await this.dep.productProvider.getProducts();
        products.forEach(product => {
            var mapped = await this.dep.productMapper.map(product);
            await this.dep.productQueuer.queue(mapped);
        });
    }
}