"use strict"

let dependencies = {
    productProvider: new (require('./productProvider'))(),
    productMapper: new (require('./productMapper'))(),
    productQueuer: new (require('./productQueuer'))()
};
var engine = new (require('./engine'))(dependencies);
engine.run();
